# Chrome Extension

To test in Dev Mode (locally) the extension:

1. Navigate to [chrome://extensions](chrome://extensions)
 
2. Enable dev mode from the top right corner:
 
 ![image](https://user-images.githubusercontent.com/4587350/116890573-740aac00-ac36-11eb-9e7d-30ff80b356b5.png)


3. Load unpacked the extension from the top left corner:
 
 ![image](https://user-images.githubusercontent.com/4587350/116890643-84bb2200-ac36-11eb-8a21-bb9e8830e9be.png)

4. Choose the "src" directory
