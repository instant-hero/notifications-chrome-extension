chrome.runtime.onInstalled.addListener(() => {
    const socket = io('http://localhost:3001/notifications', {
        transports: ['websocket']
    });

    socket.on('notification', msg => {
        console.log('new notification', msg)

        chrome.notifications.create(null, {
            type: "basic",
            title: `Случи се ${msg.type.name}!`,
            iconUrl: "img.png",
            message: `Твоята помощ е нужна! На ${msg.dateStarted.split("T")[0]} се случи ${msg.type.name} около ${msg.coordinates}. ${msg.description}`
        })
    })
});
